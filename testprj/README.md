Notes :
* To get the service use the CID. 
      var iface = Components.classesByID['{4f47e42e-4d23-4dd3-bfda-eb29255e9ea3}'].
            getService(Components.interfaces.nsIHttpProtocolHandler);

* UUID for the services can be found in compreg.dat file
  http://www2.potsdam.edu/betrusak/605/KompoZer%200.7.10/components/compreg.dat

* Match the case for contractID and classID
