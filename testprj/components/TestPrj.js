Components.utils.import('resource://gre/modules/XPCOMUtils.jsm');


function HelloWorld(){
    this.wrappedJSObject = this;
}


function logconsole(amessage){
var consoleService = Components.classes['@mozilla.org/consoleservice;1'].getService(Components.interfaces.nsIConsoleService);
    consoleService.logStringMessage(amessage);
}

HelloWorld.prototype = {
    classDescription:'My Hello World test project',
    classID:Components.ID("{0cbfefb2-5fc8-45b9-8864-29306c5efc76}"),
    QueryInterface: XPCOMUtils.generateQI([
        /* Components.interfaces.nsIObserver, */
        Components.interfaces.nsIHelloWorld]),
    hello:function(){ 
        logconsole('saying hello');
        return 'hello prasad'; 
    }
};



const NSGetFactory = XPCOMUtils.generateNSGetFactory([HelloWorld]);
