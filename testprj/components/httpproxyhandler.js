Components.utils.import('resource://gre/modules/XPCOMUtils.jsm');



function newURI(spec){
    var iface = Components.classesByID['{4f47e42e-4d23-4dd3-bfda-eb29255e9ea3}'].
            getService(Components.interfaces.nsIHttpProtocolHandler);
    return iface.newURI(spec, null, null);


}


function rewritehttp(uri){
    var patt=/astrokid\.swf/gi;
    var spec = uri.spec;
    if ( spec.match(patt)){
        return newURI('http://localhost:8080/greedy_mole.swf');
    }
    dump(uri.spec + '\n');
    return uri;
    
}


function buildProxy(src, dst) {
	  for (var f in src)
		    if (!(f in dst))
			      (function(f, dst) {
				        if (typeof(src[f]) == 'function')
					          dst[f] = function() {
						            return src[f].apply(src, arguments);
					          };
				        else {
					          dst.__defineGetter__(f, function() {
						            return src[f];
					          });
					          dst.__defineSetter__(f, function(val) {
						            src[f] = val;
					          });
				        }
			      })(f, dst);

}



function HttpProxyHandler(){
    this.wrappedJSObject=this;
}

HttpProxyHandler.prototype = {
    classDescription : 'Http handler proxy',
    classID          : Components.ID('{38defa6a-2942-40b6-b41d-e049602bc173}'),
    contractID       : '@mozilla.org/network/protocol;1?name=http',

    QueryInterface:function(iid){
        this.QueryInterface = XPCOMUtils.generateQI([
            Components.interfaces.nsISupports,
            Components.interfaces.nsIProtocolHandler, 
            Components.interfaces.nsIProxiedProtocolHandler,
            Components.interfaces.nsIHttpProtocolHandler
            ]);
        dump("HttpProxyHandler\n");

         buildProxy(this._getoriginal(), this);

        return this.QueryInterface(iid);
    },


    _getoriginal:function(){
    var iface = Components.classesByID['{4f47e42e-4d23-4dd3-bfda-eb29255e9ea3}'].
            getService(Components.interfaces.nsIHttpProtocolHandler);
        return iface;
    },


    newChannel:function(){
        var orig = this._getoriginal();
        arguments[0] = rewritehttp(arguments[0]);
        return orig.newChannel.apply(orig, arguments);
    },

    newProxiedChannel:function(){
        var orig = this._getoriginal();
        arguments[0] = rewritehttp(arguments[0]);
        return orig.newProxiedChannel.apply(orig, arguments);
        
    }

};





function HttpsProxyHandler(){
    this.wrappedJSObject=this;
};

HttpsProxyHandler.prototype = {
    classDescription     :'Https handler proxy',
    classID              : Components.ID('{a4cd68fa-d074-4469-86e1-edc56f85916a}'),
    contractID           : '@mozilla.org/network/protocol;1?name=https',

    QueryInterface:function(iid){
        this.QueryInterface = XPCOMUtils.generateQI([
            Components.interfaces.nsISupports,
            Components.interfaces.nsIProtocolHandler, 
            Components.interfaces.nsIProxiedProtocolHandler,
            Components.interfaces.nsIHttpProtocolHandler
            ]);
        dump("HttpsProxyHandler\n");
        buildProxy(this._getoriginal(), this);
        return this.QueryInterface(iid);
    },

    _getoriginal:function(){
        return Components.classesByID['{dccbe7e4-7750-466b-a557-5ea36c8ff24e}'].
            getService(Components.interfaces.nsIHttpProtocolHandler);
        
    },

    newChannel:function(){
        var orig = this._getoriginal();
        arguments[0] = rewritehttp(arguments[0]);
        return orig.newChannel.apply(orig, arguments);
    },

    newProxiedChannel:function(){
        var orig = this._getoriginal();
        arguments[0] = rewritehttp(arguments[0]);
        return orig.newProxiedChannel.apply(orig, arguments);
    }


};




const NSGetFactory = XPCOMUtils.generateNSGetFactory([HttpProxyHandler, HttpsProxyHandler]);
